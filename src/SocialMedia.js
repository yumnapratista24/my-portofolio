import { Component } from 'react';
import SOCIALMEDIAS from './data/socialMedia';

class SocialMediaList extends Component {
    render() {
        return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                {
                    SOCIALMEDIAS.map((item) => {
                        return <SocialMediaItem key={item.id} items={item} />
                    })
                }
            </div>
        )
    }
}

class SocialMediaItem extends Component {
    render() {
        const { icon, link } = this.props.items;

        return (
            <div style={{ display: 'inline-block', margin: 10 }}>
                <a href={link}>
                    <img src={icon} style={{
                        width: 25, height: 25
                    }}
                    />
                </a>
            </div>
        )
    }

}

export default SocialMediaList;
import React, { Component } from 'react';
import Profile from './Profile';
import Projects from './Project';
import SocialMediaList from './SocialMedia';
class App extends Component {
  render() {
    return (
      <div>
        <Profile />
        <Projects />
        <hr />
        <SocialMediaList />
      </div>
    );
  }
}



export default App;

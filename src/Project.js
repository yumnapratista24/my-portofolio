import { render } from "@testing-library/react";
import { Component } from "react";
import PROJECTS from "./data/project.js";

class Projects extends Component {
    render() {
        return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                {PROJECTS.map((project) => {
                    return <Project key={project.id} project={project} />
                })}
            </div>
        );
    }
}

class Project extends Component {
    render() {
        const { title, description, image, link } = this.props.project;

        return (
            <div style={{ display: "inline-block", width: 300, margin: 10 }}>
                <h3>{title}</h3>
                <img src={image} style={{ width: 200, height: 120 }} />
                <p>{description}</p>
                <a href={link} >{link}</a>
            </div>
        );
    }
}

export default Projects;
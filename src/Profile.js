import { Component } from 'react';
import './Profile.css';

class Profile extends Component {
    constructor() {
        super();
        this.state = {
            displayBio: false,
        };

        this.onTapButton = this.onTapButton.bind(this);
    }

    render() {
        return (
            this.renderProfile()
        );
    }

    onTapButton() {
        this.setState({ displayBio: !this.state.displayBio })
    }

    renderButton() {
        let buttonTitle = !this.state.displayBio ? "Show bio" : "Hide bio";
        return (
            <button
                className="btn btn-default"
                onClick={this.onTapButton}>
                {buttonTitle}
            </button>
        )
    }

    renderBio() {
        return (
            <div>
                <h1>Hi!</h1>
                <p>My name is Yumna. I'm a software engineer</p>
            </div>
        )
    }

    renderProfile() {
        return (
            <div className="Profile">
                <header className="Profile-header">
                    {(this.state.displayBio) ? this.renderBio() : null}
                    <br />
                    {this.renderButton()}
                </header>
            </div>
        );
    }
}

export default Profile;
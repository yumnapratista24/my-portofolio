import temp1 from '../assets/temp 1.jpg';
import temp2 from '../assets/temp 2.png';
import temp3 from '../assets/temp 3.jpg';

const PROJECTS = [
    {
        id: 1,
        title: "Example 1",
        description: "test 1",
        image: temp1,
        link: "https://gitlab.com",
    },
    {
        id: 2,
        title: "Example 2",
        description: "test 2",
        image: temp2,
        link: "https://gitlab.com",
    },
    {
        id: 3,
        title: "Example 3",
        description: "test 3",
        image: temp3,
        link: "https://gitlab.com",
    }
];

export default PROJECTS;
import emailIcon from '../assets/icons/email_icon.png';
import linkedinIcon from '../assets/icons/linkedin_icon.png';
import githubIcon from '../assets/icons/github_icon.png';
import twitterIcon from '../assets/icons/twitter_icon.png';

const SOCIALMEDIAS = [
    {
        id: 1,
        icon: emailIcon,
        link: 'mailto:yumnapratista24@gmail.com'
    },
    {
        id: 2,
        icon: githubIcon,
        link: 'https://gitlab.com/yumnapratista24'
    },
    {
        id: 3,
        icon: linkedinIcon,
        link: 'https://linkedin.com/in/yumnapratista'
    },
    {
        id: 4,
        icon: twitterIcon,
        link: 'https://twitter.com/yumnapratista24'
    }
]

export default SOCIALMEDIAS;